from config import get_data_path

import urllib2
import re
import datetime
import sys
import os

# Here are the URLS for each position, standard scoring
URLS = {}
URLS["RB"] = "https://s3-us-west-1.amazonaws.com/fftiers/out/current/text_RB.txt"
URLS["WR"] = "https://s3-us-west-1.amazonaws.com/fftiers/out/current/text_WR.txt"
URLS["QB"] = "https://s3-us-west-1.amazonaws.com/fftiers/out/current/text_QB.txt"
URLS["TE"] = "https://s3-us-west-1.amazonaws.com/fftiers/out/current/text_TE.txt"
URLS["DST"] = "https://s3-us-west-1.amazonaws.com/fftiers/out/current/text_DST.txt"

def get_tiers(week):
	inpad = os.path.join(get_data_path(), 'week%i' % week)
	if not os.path.exists(inpad):
		os.makedirs(inpad)

	timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M")

	for position in URLS:
		# Read the data from borischen.co
		body = urllib2.urlopen(URLS[position]).read()

		# Going to write to a csv file
		f = open(os.path.join(inpad, 'boris_tiers_%s_%s.csv' % (position, timestamp)), 'w')
		f.write("tier,name,score\n")

		# Get rows that have data
		rows = [r for r in body.split("\n") if r]
		for row in rows:
			row = row.strip()
			tname, pnames = row.split(":")
			tnum = int(re.search("Tier\s(\d+)", tname).groups()[0])
			for name in pnames.split(","):
				f.write("%s,%s,\n" % (tnum, name.strip()))
		f.close()	

if __name__ == "__main__":
	get_tiers(int(sys.argv[1]))