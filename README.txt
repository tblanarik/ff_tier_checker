This is a series of hacks to allow you to check the accuracy of the borischen.co Fantasy Football tiers.
It only evaluates ESPN standard scoring. If you want to populate the CSV files with other scores, then by all means go ahead.

Warning: It writes all data to your desktop\boris\data
If that's going to be a problem, just change it in the source.

Instructions:
1) Execute python tierget.py with the week # as the week # as the command line argument every day from Wednesday until Sunday. This will save Boris' tiers to a CSV file
2) On Tuesday morning after the scores are all in, call python getscores.py with the week # as the command line argument
3) Execute python populate_scores.py with the week # as the command line argument
4) Execute python tiereval.py with the week # as the command line argument

tiereval.py will print out a summary looking like this:

Week 2 | Position QB | Accuracy 56.25 | File boris_tiers_QB_20160922_1831.csv
Week 2 | Position WR | Accuracy 60.73 | File boris_tiers_WR_20160922_1831.csv
Week 2 | Position RB | Accuracy 71.73 | File boris_tiers_RB_20160922_1831.csv
Week 2 | Position TE | Accuracy 52.07 | File boris_tiers_TE_20160922_1831.csv