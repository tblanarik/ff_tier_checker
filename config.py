import os

def get_data_path():
	if 'HOME' in os.environ:
		return os.path.join(os.environ['HOME'], 'boris', 'data')
	elif 'HOMEPATH' in os.environ:
		return os.path.join(os.environ['HOMEPATH'], 'Desktop', 'boris', 'data')