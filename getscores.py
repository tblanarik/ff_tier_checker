from config import get_data_path
import os
import sys
import urllib2
import re

positions = {"QB": 0, "RB": 2, "WR": 4, "TE": 6}

def player_score(player_name, position, week):
	infile = os.path.join(get_data_path(), r'week%i' % week, 'scores_%s.txt' % position)
	all_data = open(infile).read()
	name_index = all_data.find(player_name.lower())
	player_content = all_data[name_index:name_index+10000] # about 2000 more characters is enough
	pt_index = player_content.find("appliedpointsprogamefinal")
	#score = player_content[pt_index+27:pt_index+29].replace("<", "").strip()
	found = re.search(">(\d+)<", player_content[pt_index:pt_index+400])
	if found:
		score = int(found.groups()[0])
	else:
		score = 0
	return score
	
def dump_all_week_data(week):
	outpad = os.path.join(get_data_path(), 'week%i' % week)
	for position in positions:
		all_data = ""
		for i in xrange(0, 300, 50):
			espn_url = 'http://games.espn.com/ffl/leaders?slotCategoryId=%i&scoringPeriodId=%i&seasonId=2016&startIndex=%i' % (positions[position], week, i)
			page_read = urllib2.urlopen(espn_url).read()
			all_data += page_read	
		all_data = all_data.lower()
		f = open(os.path.join(outpad, "scores_%s.txt" % position), 'w')
		f.write(all_data)
		f.close()
		
if __name__ == "__main__":
	dump_all_week_data(int(sys.argv[1]))