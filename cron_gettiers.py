import datetime
from tierget import get_tiers

if __name__ == "__main__":
	# Don't get tiers on Monday or Tuesday
	if not datetime.datetime.now().weekday() in [0,1]:
		w0 = datetime.datetime(2016, 8, 31)
		wn = datetime.datetime.now()
		nweeks = (wn-w0).days/7
		get_tiers(nweeks)
	