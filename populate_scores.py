from getscores import positions, player_score
from config import get_data_path
import glob
import os
import sys

def populate_boris_scores(week):
	inpad = os.path.join(get_data_path(), 'week%i' % week)
	for position in positions:
		for fname in glob.glob(os.path.join(inpad, "boris_tiers_%s_*.csv" % position)):
			populate_file(fname, position, week)
			
def populate_file(fname, position, week):
	rows = open(fname).read().split("\n")
	header = rows[0]
	data = []
	for row in rows[1:]:
		tmp = row.split(",")
		if len(tmp) < 3:
			continue
		tmp[2] = player_score(tmp[1], position, week)
		data.append(tmp)
	f = open(fname, 'w')
	f.write("%s\n" % header)
	for d in data:
		f.write("%s,%s,%s\n" % (d[0], d[1], d[2]))
	f.close()
	
	
if __name__ == "__main__":
	populate_boris_scores(int(sys.argv[1]))