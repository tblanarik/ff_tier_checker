import sys
from config import get_data_path
from getscores import positions, player_score
import glob
import os

def evaluate(fname):
	# Load our file
	f = open(fname)

	# Toss the header "tier,name,score"
	f.readline()

	data = []

	# Read in the data and clean it up just a bit
	for row in f: 
		tier,name,points = row.split(",")
		data.append([int(tier), name.strip(), float(points)])
	f.close()

	right_count = 0
	total = 0

	for i in xrange(len(data)): # Go through all of the players
		for j in xrange(len(data)): # And compare them to every other player
			if data[i][0] < data[j][0]: # There's a tier difference. Let's check it out
				total += 1 # Add 1 to the total number of tier differences we've found
				if data[i][2] > data[j][2]: # If the higher tiered player had a greater score
					right_count += 1 # then we add 1 to right_count
					
	# Isn't the accuracy just the number Boris tired correctly
	# divided by the total number of tier differences we saw?				
	#print right_count, total
	#print float(right_count)/total
	return right_count, total 
	
def evaluate_week(week):
	inpad = os.path.join(get_data_path(), 'week%i' % week)
	print inpad
	for position in positions:
		for fname in glob.glob(os.path.join(inpad, "boris_tiers_%s_*.csv" % position)):
			rc, total = evaluate(fname)
			acc = round(float(rc)/total*100.0, 2)
			print "\tWeek %s | Position %s | Accuracy %s | File %s" % (week, position, acc, os.path.basename(fname))
	
if __name__ == "__main__":
	evaluate_week(int(sys.argv[1]))